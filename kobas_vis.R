
pacman::p_load(ggplot2, dplyr, tibble, formattable)


T_vs_S_all_DEGs <- read.csv("/home/paddy/2021_09_thuenen_institut/KOBAS/1_T_vs_S_DEGs_Qsuber_4035_geneIDs_KOBAS_output.txt", sep = "\t", skip=6, nrows = 113) %>%
   as_tibble() %>% rename(Pathway.name =X.Term) %>%
   mutate_at(vars(Pathway.name), as.factor) 
  
plot_set <- filter(T_vs_S_all_DEGs, T_vs_S_all_DEGs$Corrected.P.Value < 0.05)

ggplot(plot_set, aes(y= Corrected.P.Value, x = Pathway.name)) + coord_flip() +
  geom_point(aes(color = Corrected.P.Value, size = Input.number), alpha = 0.5) +
  scale_color_gradient(low = "yellow", high = "red", na.value = NA) +
  scale_size(range = c(1, 10)) + # Adjust the range of points size
  theme_set(theme_bw() +theme(legend.position = "bottom"))

